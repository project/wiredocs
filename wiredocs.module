<?php
/**
 * @file wiredocs.module
 * Wiredocs allows content editors to download and edit a file locally. If the (local) file
 * is changed the file is uploaded again to the original remote location. This module provides
 * the server-side integration which is used by the client-side Java applet.
 */

/**
 * Implements hook_menu()
 */
function wiredocs_menu(){

  $items = array();

  $items['wiredocs/%file/download'] = array(
    'page callback' => 'wiredocs_download',
    'page arguments' => array(1),
    'access callback' => 'wiredocs_access',
    'access arguments' => array(2, 1),
    'type' => MENU_CALLBACK,
    'file' => 'wiredocs.handler.inc',
  );

  $items['wiredocs/%file/upload'] = array(
    'page callback' => 'wiredocs_upload',
    'page arguments' => array(1),
    'access callback' => 'wiredocs_access',
    'access arguments' => array(2, 1),
    'type' => MENU_CALLBACK,
    'file' => 'wiredocs.handler.inc',
  );
  
  $items['wiredocs/jar/%'] = array(
    'page callback' => 'wiredocs_get_jar',
    'page arguments' => array(2), // jar filename with file extension
    'access callback' => TRUE, // allow for anybody, otherwise applet cannot bootstrap
    'access arguments' => array('use wiredocs'),
    'type' => MENU_CALLBACK,
    'file' => 'wiredocs.handler.inc',
  );
  
  return $items;
}

/**
 * Implements hook_element_info().
 *
 * The wiredocs form elements are globally available.
 */
function wiredocs_element_info() {  
  $elements['wiredocs_button'] = array(
    '#process' => array('wiredocs_button_process'), // themes button and applet
    '#theme' => 'wiredocs_button',
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'wiredocs') . '/wiredocs.js'), // starts the applet, i. e. file download
    ),
     
      
  );
  return $elements;
}

/**
 * Processing callback for the edit button.
 */
function wiredocs_button_process($element, &$form_state, $form) {
  if ($element['#file'] && $element['#file']->fid) {
    
    // attach invisible applet to suffix
    $applet = theme('wiredocs_applet',  array('file' => $element['#file']));
    isset($element['#suffix']) ? $element['#suffix'] .= $applet : $element['#suffix'] = $applet;
    
    // On multiple field instances we want the "Edit" button to appear in the operations column.
    // Therefore we have to spoof theme_file_widget_multiple() that it's a submit button, so
    // that it arranges the button properly.
    $element['#type'] = 'submit';
    return $element;
  }
}

/**
 * Implements hook_theme().
 * 
 * Themes for edit button and Java Applet are defined here. We keept them separate in case
 * a front end developer wants to change the edit button or make the applet visible.
 */
function wiredocs_theme($existing, $type, $theme, $path) {

  $base = array(
    'file' => 'theme.inc',
    'path' => $path . '/theme',
  );
  
  $hooks['wiredocs_button'] = $base + array(
    'variables' => array('file' => NULL, 'title' => NULL),
    'template' => 'wiredocs-button',
  );
  // invisible applet by default, processing business logic
  $hooks['wiredocs_applet'] = $base + array(
    'variables' => array('file' => NULL),
    'template' => 'wiredocs-applet',
  );
  
  return $hooks;
}



/**
 * Implements hook_permission().
 * 
 * Restricts access to wiredocs functionality by user role.
 */
function wiredocs_permission(){
  return array(
    'use wiredocs' => array(
      'title' => t('Allow edit files local'),
      'description' => t('Allow download, local editing and upload of files.'),
    ),
  );
}

/**
 * Callback determining wheter a user may apply certain operations on a file.
 * @param $op
 *   file operation
 * @param $file
 *   file object
 * @return
 *   boolean indicating if access is granted
 */
function wiredocs_access($op, $file) {
  foreach (module_implements('wiredocs_access') as $module) {
    $func = $module . '_wiredocs_access';
    if (function_exists($func)) {
      $access = $func($op, $file);
      if (!$access) return $access;
    }
  }
  return TRUE;
}

/**
 * Implements hook_wiredocs_access().
 * 
 * Checks built-in wiredocs priviliges.
 */
function wiredocs_wiredocs_access($op, $file) {
  return user_access('use wiredocs');
}